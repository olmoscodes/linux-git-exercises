
DEST="backup/marc_olmos/2021/02/03"

if [[ -d "$DESTc" ]]
    then
        cp "punto1.txt" "backup/marc_olmos/2021/02/03"
        cd backup/marc_olmos/2021/02/03/
        today=$(date +%u)
        if [[ $today == 5 ]] 
        then
            cp "punto1.txt" "nginx_logs_requests$(date -v+3d +%F).log"
            cp "punto1.txt" "nginx_logs_requests$(date -v+4d +%F).log"
            cp "punto1.txt" "nginx_logs_requests$(date -v+5d +%F).log"
            cp "punto1.txt" "nginx_logs_requests$(date -v+6d +%F).log"
            cp "punto1.txt" "nginx_logs_requests$(date -v+7d +%F).log"
            rm -f punto1.txt

            tar -cvzf nginx_logs_requests_$(date -v+7d +%F).tar.gz ./*
        fi
else
    mkdir -p "backup/marc_olmos/2021/02/03"
fi


